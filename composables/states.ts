import i18next, { t, TFunction } from 'i18next';

type lng = 'en' | 'ua';

interface LanguageState {
  language: lng;
  pages: {
    index: TFunction,
    info: TFunction
  }
}

export const useLanguageStore = defineStore('language', {
  state: (): LanguageState => ({ language: 'en', pages: { index: t('index'), info: t('info') } }),
  actions: {
    update(currentState: lng = 'en') {
      console.log('update was called', this.language, currentState);
      if (currentState === 'en') {
        i18next
          .changeLanguage('ua')
          .then((t: TFunction) => {
            this.pages = { index: t('index'), info: t('info') };
          })
          .catch((e) => console.error(e));
        this.language = 'ua';
      } else {
        i18next
          .changeLanguage('en')
          .then((t: TFunction) => {
            this.pages = { index: t('index'), info: t('info') };
          });
        this.language = 'en';
      }
    }
  }
})

